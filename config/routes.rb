Rails.application.routes.draw do
  namespace :api do
    namespace :v1 do
      post 'reset_password' => 'reset_password#password_reset'
      # post 'update_password' => 'reset_password#update_password'

      mount_devise_token_auth_for 'User', at: 'auth'

      # HomeController
      get 'all_data' => 'home#index'
      get 'relations' => 'home#relations'
      get 'hospitals' => 'home#hospitals'
      get 'tips' => 'home#tips'

      # post 'add_graph' => 'graphs#create'
      resources :graphs
      post 'update_graphs' => 'graphs#bulk_update'

      resources :emergency
      post 'update_emergencies' => 'emergency#bulk_update'
    end
  end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
