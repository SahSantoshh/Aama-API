class Graph < ActiveRecord::Base
  before_validation :set_uuid, on: :create
  belongs_to :user


   def set_uuid
     self.id = SecureRandom.uuid
   end
end
