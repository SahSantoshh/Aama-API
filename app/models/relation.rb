class Relation < ActiveRecord::Base
  before_validation :set_uuid, on: :create
  validates :name, uniqueness: true
  has_many :emergencies

 def set_uuid
   self.id = SecureRandom.uuid
 end
end
