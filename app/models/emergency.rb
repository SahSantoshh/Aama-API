class Emergency < ActiveRecord::Base
  before_validation :set_uuid, on: :create
  belongs_to :user
  belongs_to :relation

  # validates_associated :user, :message => "You have already too much emergency contacts."

   def set_uuid
     self.id = SecureRandom.uuid
   end
end
