class User < ActiveRecord::Base
  before_validation :set_uuid, on: :create
  has_many :emergencies
  has_many :graphs

  # Include default devise modules.
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable,
         authentication_keys: [:email]
  # :confirmable
  # :omniauthable

  include DeviseTokenAuth::Concerns::User
  validates :lmp, presence: true
  validates :id, presence: true
  validates :mom, presence: true

  has_attached_file :avatar,
                    styles: { medium: '300x300>', thumb: '100x100>' },
                    url: '/profile/:id/:style/:filename',
                    default_url: '/images/aama.jpg'

  validates_attachment_content_type :avatar, content_type: /\Aimage\/.*\Z/

  def set_uuid
    self.id = SecureRandom.uuid
  end

  def send_reset_password_instructions(opts = nil)
    token = genrate_custom_token
    opts ||= {}
    # fall back to "default" config name
    opts[:client_config] ||= 'default'

    send_devise_notification(:reset_password_instructions, token, opts)
  end

  def genrate_custom_token
    token = SecureRandom.base58(8)
    token_enc = Devise.token_generator.digest(self, :reset_password_token, token)
    self.reset_password_token   = token_enc
    self.reset_password_sent_at = Time.now.utc
    save(validate: false)
    token
  end

  def update_password(password)
    self.password = password
    save
  end
end
