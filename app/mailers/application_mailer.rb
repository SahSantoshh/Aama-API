class ApplicationMailer < ActionMailer::Base
  default from: 'Aama app'
  layout 'mailer'
end
