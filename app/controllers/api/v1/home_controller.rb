module Api
  module V1
    class HomeController < ApiController
      before_action :authenticate_api_v1_user!

      def index
        rels = Relation.all
        hos = Hospital.all
        ti = Tip.all
        eme = Emergency.where(user_id: current_api_v1_user.id)
        gra = Graph.where(user_id: current_api_v1_user.id)
        response = {
          status: 'success',
          relations: rels,
          hospitals: hos,
          tips: ti,
          emergency: eme,
          graphs: gra
        }

        render json: response
      end

      def relations
        rels = Relation.all
        response = {
          status: 'success',
          relations: rels
        }
        render json: response
      end

      def hospitals
        hos = Hospital.all
        render json: hos
      end

      def tips
        hos = Tip.all
        render json: hos
      end
    end
  end
end
