module Api
  module V1
    class EmergencyController < ApiController
      before_action :authenticate_api_v1_user!

      def index
        emegencies = Emergency.where(user_id: current_api_v1_user.id)

        render json: emegencies
      end

      def create
        emergencies = emergencies_params[:emergency]
        batch_insert = Emergency.create(emergencies)

        if batch_insert
          render json: {
            status: :created,
            emergencies: batch_insert
          }
        else
          render json: {
            status: :unprocessable_entity,
            emergencies: batch_insert
          }
        end
      end

      def bulk_update
       updated_records = []
       emergencies = bulk_update_params[:emergency]
       emergencies.each do |emergency|
         updated_records << update_one_record_at_a_time(emergency)
       end

       render json: updated_records
     end

     # DELETE /emergency/1
     def destroy
        Emergency.find(params[:id]).destroy

        render json: {
          seccess: true,
          message: 'Emergency contact successfully deleted'
        }
     end
     #
    #  def bulk_destroy
    #     if Emergency.find(params[:ids])
    #       ActiveRecord::Base.transaction do
    #         Emergency.where(id: params[:ids]).destroy_all
    #       end
    #       render json: {
    #           success: true,
    #           status: 200,
    #           message: 'Emergency contacts successfully destroyed'
    #       }
     #
    #     else
    #       render json:{
    #           success: false,
    #           message: 'Emergency contacts destroy failure'
    #       }
    #     end
    #   end


      private
      def update_one_record_at_a_time update_params
        emergency = Emergency.where(:id => update_params[:id])

        if !emergency.any?
          update_params[:not_found] = "Graph not found"
         return update_params
        else
          emergency = emergency.first
          if  emergency.update_attributes(update_params)
            return emergency
          else
            update_params[:error] = "Update failed"
            return update_params
          end
        end
      end

      def parms_for_delete
        params.permit(:ids => [:id])
      end

      def update_params
        params.require(:emergency).permit(:id, :name, :relation_id, :user_id, :contact )
      end

      def bulk_update_params
        params.permit(:format, emergency: [:id, :name, :relation_id, :user_id, :contact ])
      end

      def emergencies_params
        params.permit(:format, emergency: [:name, :relation_id, :user_id, :contact ])

      end
    end
  end
end
