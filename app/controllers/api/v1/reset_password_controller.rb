module Api
  module V1
    class ResetPasswordController < ApiController
      before_action :authenticate_api_v1_user!, except: [:password_reset]

      def password_reset
        resetCode = reset_password_params[:resetCode]
        password = reset_password_params[:password]

        reset_password_token = Devise.token_generator.digest(self, :reset_password_token, resetCode)
        user = User.find_by reset_password_token: reset_password_token
        if user.nil?
          return render json: { success: false, notice: 'Unable to find user' }
        end
        if user.reset_password_sent_at < 1.hour.ago
          return render json: { success: false, notice: 'Password Token has now expired' }
        elsif user.update_password(password)
          return render json: { success: true, notice: 'Password has been updated' }
        else
          return render json: { success: false, notice: 'Unknown error has occured' }
        end
      end

      protected

      def reset_password_params
        params.permit(:resetCode, :password, :password_confirmation)
      end
    end
  end
end
