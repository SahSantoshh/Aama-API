module Api
  module V1
    class GraphsController < ApiController
      before_action :authenticate_api_v1_user!

      def index
        grphs = Graph.where(user_id: current_api_v1_user.id)
        render json: grphs
      end

      def create
        graphs = graphs_params[:graph]
        batch_insert = Graph.create(graphs)

        if batch_insert
          render json: {
            status: :created,
            graphs: batch_insert
          }
        else
          render json: {
            status: :unprocessable_entity,
            graphs: batch_insert
          }
        end
      end

      def bulk_update
        updated_graphs = []
        graphs = bulk_update_params[:graph]

        graphs.each do |graph|
          updated_graphs << update_one_record_at_a_time(graph)
        end

        render json: updated_graphs
      end

      private

      def update_one_record_at_a_time(update_params)
        graph = Graph.where(id: update_params[:id])

        if graph.none?
          update_params[:not_found] = 'Graph not found'
          return update_params
        else
          graph = graph.first
          if graph.update_attributes(update_params)
            return graph
          else
            update_params[:error] = 'Update failed'
            return update_params
          end
        end
      end

      def update_params
        params.require(:graph).permit(:id, :user_id, :week_no, :weight, :bp)
      end

      def bulk_update_params
        params.permit(:format, graph: %i[id user_id week_no weight bp])
      end

      def graphs_params
        params.permit(:format, graph: %i[user_id week_no weight bp])
      end
    end
  end
end
