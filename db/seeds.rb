# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

# Hospital Migration
# class MakeHopitalsColumnToNullTrue < ActiveRecord::Migration[5.0]
#   def up
#     change_column :hospitals, :name, :string, :null => true
#     change_column :hospitals, :contact, :bigint, :null => true
#     change_column :hospitals, :address, :string, :null => true
#
#   end
#
#   def down
#     change_column :hospitals, :name, :string, :null => false, default: "Aama Hospital"
#     change_column :hospitals, :contact, :bigint, :null => false, default: "98989898"
#     change_column :hospitals, :address, :string, :null => false, default: "Nepal"
#
#   end
# end

Relation.create(name: 'Husband')
Relation.create(name: 'Hospital')
Relation.create(name: 'Family')

url = 'https://docs.google.com/spreadsheets/d/14dCyrfnBXtj-Nibb6uYHa38n96eYO2NFRiPN3Uqhr_w/export?format=xlsx'
xls = Roo::Spreadsheet.open(url, extension: :xlsx)
xls.each do |row|
  Hospital.create(name: row[0], contact: row[1], address: row[2])
end

url_tips = 'https://docs.google.com/spreadsheets/d/1bbsRAonY3VG3rQfRC3-5HxskGkWfD1nuVq88U3Y_0L0/export?format=xlsx'
xls_tips = Roo::Spreadsheet.open(url_tips, extension: :xlsx)
xls_tips.each do |row|
  Tip.create(title: row[0], description: row[1], avatar_name: row[2], week_no: row[3], np_title: row[6], np_description: row[7], health: false)
end

health_tips = xls_tips.sheet(1)
health_tips.each do |row|
  Tip.create(title: row[0], description: row[1], avatar_name: row[2], week_no: row[3], np_title: row[6], np_description: row[7], health: true)
end
