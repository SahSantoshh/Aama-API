class AddColumnMomToUser < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :mom, :boolean, default: true
  end
end
