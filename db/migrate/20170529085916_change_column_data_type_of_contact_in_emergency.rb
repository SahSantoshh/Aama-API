class ChangeColumnDataTypeOfContactInEmergency < ActiveRecord::Migration[5.0]
  def change
    change_column :emergencies, :contact, :bigint
  end
end
