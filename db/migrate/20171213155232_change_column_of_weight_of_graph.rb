class ChangeColumnOfWeightOfGraph < ActiveRecord::Migration[5.0]
  def up
    change_column :graphs, :weight, :float
  end

  def down
    change_column :graphs, :weight, :integer
  end
end
