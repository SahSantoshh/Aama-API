class AddColumnImageNameToTip < ActiveRecord::Migration[5.0]
  def self.up  
    add_column :tips, :avatar_name, :string
  end  

  def self.down  
    remove_column :tips, :avatar_name
  end  
end
