class CreateHospital < ActiveRecord::Migration[5.0]
  def change
    create_table :hospitals, id: false, force: true do |t|
      t.string :id, :limit => 36, :primary_key => true
      t.string :name, :null => false
      t.integer :contact, :null => false
      t.string :address, :null => false

      t.timestamps
    end
  end
end
