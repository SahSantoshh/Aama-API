class CreateGraph < ActiveRecord::Migration[5.0]
  def change
    create_table :graphs, id: false, force: true do |t|
      t.string :id, :limit => 36, :primary_key => true
      t.integer :week_no, :null => false, :default => 1
      t.integer :weight
      t.string :bp # systolic/dystolic 100/80

      t.timestamps
    end
  end
end
