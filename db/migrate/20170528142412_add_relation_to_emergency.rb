class AddRelationToEmergency < ActiveRecord::Migration[5.0]
  def change
    add_reference :emergencies, :relation, foreign_key: true, type: :string
  end
end
