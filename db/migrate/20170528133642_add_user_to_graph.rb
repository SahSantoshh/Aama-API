class AddUserToGraph < ActiveRecord::Migration[5.0]
  def change
    add_reference :graphs, :user, foreign_key: true, type: :string
  end
end
