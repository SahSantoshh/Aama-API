class ChangeColumnDataTypeOfContactInHospital < ActiveRecord::Migration[5.0]
  def change
    change_column :hospitals, :contact, :bigint
  end
end
