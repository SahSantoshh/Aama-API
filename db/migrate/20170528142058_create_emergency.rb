class CreateEmergency < ActiveRecord::Migration[5.0]
  def change
    create_table :emergencies, id: false, force: true do |t|
      t.string :id, :limit => 36, :primary_key => true
      t.integer :contact, :null => false
      t.string :name, :null => false

      t.timestamps
    end
  end
end
