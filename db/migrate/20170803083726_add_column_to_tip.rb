class AddColumnToTip < ActiveRecord::Migration[5.0]
  def change
    add_column :tips, :np_title, :string
    add_column :tips, :np_description, :text
  end
end
