class ChangeColumnTypeWeekNoOfGraph < ActiveRecord::Migration[5.0]
  def change
    change_column :graphs, :week_no, :integer
  end
end
