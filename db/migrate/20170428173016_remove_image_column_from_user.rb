class RemoveImageColumnFromUser < ActiveRecord::Migration[5.0]
  def self.up
    remove_column :users, :image
    add_column :users, :lmp, :date
  end

  def self.down
    add_column :users, :image, :string
    remove_column :users, :lmp
  end
end
