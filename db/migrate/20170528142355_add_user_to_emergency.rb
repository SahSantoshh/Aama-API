class AddUserToEmergency < ActiveRecord::Migration[5.0]
  def change
    add_reference :emergencies, :user, foreign_key: true, type: :string
  end
end
