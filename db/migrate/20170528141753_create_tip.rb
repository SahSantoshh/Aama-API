class CreateTip < ActiveRecord::Migration[5.0]
  def change
    create_table :tips, id: false, force: true do |t|
      t.string :id, :limit => 36, :primary_key => true
      t.string :title, :null => false, :default => ""
      t.text :description
      t.integer :week_no, :null => false, :default => 1
      t.boolean :hf, :null => false, :default => true # health true, food false

      t.timestamps
    end
  end
end
