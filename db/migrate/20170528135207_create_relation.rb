class CreateRelation < ActiveRecord::Migration[5.0]
  def change
    create_table :relations, id: false, force: true do |t|
      t.string :id, :limit => 36, :primary_key => true
      t.string :name, :null => false
      t.timestamps
    end
  end
end
