class ChangeColummnHfToHealthOOfTips < ActiveRecord::Migration[5.0]
  def change
    rename_column :tips, :hf, :health
  end
end
